"""
Created on 20th Dec, 2018
https://tkdocs.com/tutorial/grid.html
"""

from tkinter import *
from typing import List


def cage(canvas: Canvas, line_distance: int) -> None:
    """Create checkered canvas - board-home for cells."""
    canvas.update_idletasks()
    c_width = canvas.winfo_width()
    c_height = canvas.winfo_height()
    # vertical lines at an interval of "line_distance" pixel:
    for x in range(line_distance, c_width, line_distance):
        canvas.create_line(x, 0, x, c_height, fill="#B2B2B2")
    # horizontal lines at an interval of "line_distance" pixel
    for y in range(line_distance, c_height, line_distance):
        canvas.create_line(0, y, c_width, y, fill="#B2B2B2")


def render(board: List[List[int]], canvas: Canvas, line_distance: int) -> None:
    """Fill in the board cells according to the given "0/1" matrix using Tk GUI.
    """
    for i in range(len(board)):
        for j in range(len(board[i])):
            if board[i][j] == 1:
                x1 = j * line_distance
                y1 = i * line_distance
                x2 = x1 + line_distance
                y2 = y1 + line_distance
                canvas.create_rectangle(x1, y1, x2, y2, outline="grey",
                                        fill="black", tag="alive")
