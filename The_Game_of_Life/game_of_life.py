"""
Created in the period from 10th to 23th Dec, 2018

@author: Mary Dmitrieva - dmimaryt@gmail.com
"""

from random import randint
from typing import List


def dead_state(width: int, height: int) -> List[List[int]]:
    """Return the matrix initialised by 0. Board with no one alive cell."""
    return [[0 for i in range(height)] for j in range(width)]


def random_state(width: int, height: int) -> List[List[int]]:
    """Return the 2D-list to build a board of random state of each cell
    to either 0 or 1 (dead or alive)"""
    return [[randint(0, 1) for row in range(width)] for col in range(height)]


def load_board_state(file_name: str) -> List[List[int]]:
    """Load the initial state from text file."""
    with open(file_name, 'r') as file:
        # read_data = file.read()
        # A readlines() reads until EOF using readline() and returns a list
        # containing the lines.
        data = file.readlines()
    rows = len(data)
    cols = len(data[0]) - 1
    result = dead_state(rows, cols)
    for i in range(rows):
        for j in range(cols):
            result[i][j] = int(data[i][j])

    return result


# TODO: make processing of cell-neighbors shorter
def next_board_state(board: List[List[int]]) -> List[List[int]]:
    """Calculate and return the next board state, according to the rules of
     Game of Life*"""
    count = 0
    rows = len(board)
    cols = len(board[0])
    # shift = [0, 1, -1]
    new_state = dead_state(rows, cols)

    for i in range(rows):
        for j in range(cols):
            # Counting alive neighbors-cells around the current one:
            if j == 0:
                for k in range(2):  # [0, 1]
                    for m in range(-1, 2):  # [-1, 0, 1]
                        if (k, m) == (0, 0) or (i == 0 and m == -1) \
                                            or (i == rows - 1 and m == 1):
                            continue
                        if board[i + m][j + k] == 1:
                            count += 1
                        # [i-1][j], [i+1][j] | [i-1][j+1], [i][j+1], [i+1][j+1]
            elif j == cols - 1:
                for k in range(-1, 1):  # [-1, 0]
                    for m in range(-1, 2):  # [-1, 0, 1]
                        if (k, m) == (0, 0) or (i == 0 and m == -1) \
                                            or (i == rows - 1 and m == 1):
                            continue
                        if board[i + m][j + k] == 1:
                            count += 1
                        # [i-1][j-1], [i][j-1], [i+1][j-1] | [i-1][j], [i+1][j]
            elif i == 0:
                for k in range(2):  # [0, 1]
                    for m in range(-1, 2):  # [-1, 0, 1]
                        if (k, m) == (0, 0) or j == 0 or j == cols - 1:
                            continue
                        if board[i + k][j + m] == 1:
                            count += 1
                        # [i][j-1], [i][j+1] | [i+1][j-1], [i+1][j], [i+1][j+1]
            elif i == rows - 1:
                for k in range(-1, 1):  # [-1, 0]
                    for m in range(-1, 2):  # [-1, 0, 1]
                        if (k, m) == (0, 0) or j == 0 or j == cols - 1:
                            continue
                        if board[i + k][j + m] == 1:
                            count += 1
                        # [i-1][j-1], [i-1][j], [i-1][j+1] | [i][j-1], [i][j+1]
            else:
                for k in range(-1, 2):  # [-1, 0, 1]
                    for m in range(-1, 2):  # [-1, 0, 1]
                        if (k, m) == (0, 0):
                            continue
                        if board[i + m][j + k] == 1:
                            count += 1

            # Condition* check for current cell:
            if board[i][j] == 0:
                if count == 3:
                    new_state[i][j] = 1
            elif count == 2 or count == 3:
                new_state[i][j] = 1
            count = 0

    return new_state

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# *
# Any live cell with 0 or 1 live neighbors becomes dead
# Any live cell with 2 or 3 live neighbors stays alive
# Any live cell with more than 3 live neighbors becomes dead
# Any dead cell with exactly 3 live neighbors becomes alive
