"""Unit-test suites to test the next_board_state function."""

from game_of_life import *


def test_state1() -> None:
    """Test 1: dead cells with no live neighbors should stay dead."""
    init_state1 = [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0]
    ]

    expected_next_state1 = [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0]
    ]

    assert next_board_state(init_state1) == expected_next_state1


def test_state2() -> None:
    """Test 2: live cell with 0 or 1 live neighbors becomes dead."""
    expected_next_state = [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0]
    ]

    init_state = dead_state(3, 3)

    # nested for-loop for cases like this one:
    # init_state1 = [
    #     [1, 0, 0],
    #     [0, 0, 0],
    #     [0, 0, 0]
    # ]

    for i in range(3):
        for j in range(3):
            init_state[i][j] = 1
            assert next_board_state(init_state) == expected_next_state
            init_state[i][j] = 0

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # nested for-loop for cases like this one:
    # init_state = [
    #     [0, 1, 0],
    #     [0, 1, 0],
    #     [0, 0, 0]
    # ]

    for i in range(2):
        for j in range(2):
            init_state[i][j] = 1
            init_state[i + 1][j] = 1
            assert next_board_state(init_state) == expected_next_state
            init_state[i][j] = 0
            init_state[i + 1][j] = 0

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # nested for-loop for cases like this one:
    # init_state = [
    #     [0, 1, 0],
    #     [0, 0, 0],
    #     [0, 1, 0]
    # ]
    for i in range(1):
        for j in range(3):
            init_state[i][j] = 1
            init_state[i + 2][j] = 1
            assert next_board_state(init_state) == expected_next_state
            init_state[i][j] = 0
            init_state[i + 2][j] = 0

    # nested for-loop for cases like this one:
    # init_state = [
    #     [1, 0, 1],
    #     [0, 0, 0],
    #     [0, 0, 0]
    # ]
    for j in range(1):
        for i in range(3):
            init_state[i][j] = 1
            init_state[i][j + 2] = 1
            assert next_board_state(init_state) == expected_next_state
            init_state[i][j] = 0
            init_state[i][j + 2] = 0

        init_state = [
            [1, 0, 0],
            [0, 0, 0],
            [0, 0, 1]
        ]

        assert next_board_state(init_state) == expected_next_state

        init_state = [
            [1, 0, 0],
            [0, 0, 0],
            [0, 0, 1]
        ]

        assert next_board_state(init_state) == expected_next_state


def test_state3() -> None:
    """Test 3: dead cells with exactly 3 live neighbors should come alive"""
    init_state = [
        [0, 1, 1],
        [0, 0, 1],
        [0, 0, 0]
    ]

    expected_next_state = [
        [0, 1, 1],
        [0, 1, 1],
        [0, 0, 0]
    ]

    assert next_board_state(init_state) == expected_next_state

    init_state = [
        [1, 1, 0],
        [1, 0, 0],
        [0, 0, 0]
    ]

    expected_next_state = [
        [1, 1, 0],
        [1, 1, 0],
        [0, 0, 0]
    ]

    assert next_board_state(init_state) == expected_next_state

    init_state = [
        [0, 0, 0],
        [0, 0, 1],
        [0, 1, 1]
    ]

    expected_next_state = [
        [0, 0, 0],
        [0, 1, 1],
        [0, 1, 1]
    ]

    assert next_board_state(init_state) == expected_next_state

    init_state = [
        [0, 0, 0],
        [1, 0, 0],
        [1, 1, 0]
    ]

    expected_next_state = [
        [0, 0, 0],
        [1, 1, 0],
        [1, 1, 0]
    ]

    assert next_board_state(init_state) == expected_next_state


def test_state4() -> None:
    """Test 4: live cell with 2 or 3 live neighbors stays alive."""
    init_state = dead_state(3, 3)

    # nested for-loop for cases like this one:
    # init_state = [
    #     [1, 1, 0],
    #     [1, 1, 0],
    #     [0, 0, 0]
    # ]

    for k in range(2):
        for l in range(2):
            for i in range(2):
                for j in range(2):
                    init_state[i + k][j + l] = 1
            assert next_board_state(init_state) == init_state
            init_state = dead_state(3, 3)


def test_state5() -> None:
    """Test 5: mixed cases."""
    init_state = [
        [1, 0, 0],
        [1, 0, 0],
        [1, 0, 0]
    ]

    expected_next_state = [
        [0, 0, 0],
        [1, 1, 0],
        [0, 0, 0]
    ]

    assert next_board_state(init_state) == expected_next_state

    init_state = [
        [0, 1, 0],
        [0, 1, 0],
        [0, 1, 0]
    ]

    expected_next_state = [
        [0, 0, 0],
        [1, 1, 1],
        [0, 0, 0]
    ]

    assert next_board_state(init_state) == expected_next_state

    init_state = [
        [0, 0, 1],
        [0, 0, 1],
        [0, 0, 1]
    ]

    expected_next_state = [
        [0, 0, 0],
        [0, 1, 1],
        [0, 0, 0]
    ]

    assert next_board_state(init_state) == expected_next_state

    init_state = [
        [1, 1, 1],
        [0, 0, 0],
        [0, 0, 0]
    ]

    expected_next_state = [
        [0, 1, 0],
        [0, 1, 0],
        [0, 0, 0]
    ]

    assert next_board_state(init_state) == expected_next_state

    init_state = [
        [0, 0, 0],
        [1, 1, 1],
        [0, 0, 0]
    ]

    expected_next_state = [
        [0, 1, 0],
        [0, 1, 0],
        [0, 1, 0]
    ]

    assert next_board_state(init_state) == expected_next_state

    init_state = [
        [0, 0, 0],
        [0, 0, 0],
        [1, 1, 1]
    ]

    expected_next_state = [
        [0, 0, 0],
        [0, 1, 0],
        [0, 1, 0]
    ]

    assert next_board_state(init_state) == expected_next_state

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    init_state = [
        [1, 0, 0],
        [0, 1, 0],
        [0, 0, 1]
    ]

    expected_next_state = [
        [0, 0, 0],
        [0, 1, 0],
        [0, 0, 0]
    ]

    assert next_board_state(init_state) == expected_next_state

    init_state = [
        [0, 0, 1],
        [0, 1, 0],
        [1, 0, 0]
    ]

    assert next_board_state(init_state) == expected_next_state

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    init_state = [
        [0, 1, 1],
        [0, 0, 0],
        [1, 1, 0]
    ]

    expected_next_state = [
        [0, 0, 0],
        [1, 0, 1],
        [0, 0, 0]
    ]

    assert next_board_state(init_state) == expected_next_state

    init_state = [
        [1, 1, 0],
        [0, 0, 0],
        [0, 1, 1]
    ]

    assert next_board_state(init_state) == expected_next_state

    init_state = [
        [0, 0, 1],
        [1, 0, 1],
        [1, 0, 0]
    ]

    expected_next_state = [
        [0, 1, 0],
        [0, 0, 0],
        [0, 1, 0]
    ]

    assert next_board_state(init_state) == expected_next_state

    init_state = [
        [1, 0, 0],
        [1, 0, 1],
        [0, 0, 1]
    ]

    assert next_board_state(init_state) == expected_next_state

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    init_state = [
        [0, 1, 1],
        [1, 1, 0],
        [0, 0, 0]
    ]

    expected_next_state = [
        [1, 1, 1],
        [1, 1, 1],
        [0, 0, 0]
    ]

    assert next_board_state(init_state) == expected_next_state

    init_state = [
        [1, 1, 0],
        [0, 1, 1],
        [0, 0, 0]
    ]

    assert next_board_state(init_state) == expected_next_state

    init_state = [
        [0, 0, 0],
        [0, 1, 1],
        [1, 1, 0]
    ]

    expected_next_state = [
        [0, 0, 0],
        [1, 1, 1],
        [1, 1, 1]
    ]

    assert next_board_state(init_state) == expected_next_state

    init_state = [
        [0, 0, 0],
        [1, 1, 0],
        [0, 1, 1]
    ]

    assert next_board_state(init_state) == expected_next_state

    init_state = [
        [0, 1, 0],
        [1, 1, 0],
        [1, 0, 0]
    ]

    expected_next_state = [
        [1, 1, 0],
        [1, 1, 0],
        [1, 1, 0]
    ]

    assert next_board_state(init_state) == expected_next_state

    init_state = [
        [1, 0, 0],
        [1, 1, 0],
        [0, 1, 0]
    ]

    assert next_board_state(init_state) == expected_next_state

    init_state = [
        [0, 0, 1],
        [0, 1, 1],
        [0, 1, 0]
    ]

    expected_next_state = [
        [0, 1, 1],
        [0, 1, 1],
        [0, 1, 1]
    ]

    assert next_board_state(init_state) == expected_next_state

    init_state = [
        [0, 1, 0],
        [0, 1, 1],
        [0, 0, 1]
    ]

    assert next_board_state(init_state) == expected_next_state

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    init_state = [
        [1, 0, 0],
        [1, 0, 0],
        [1, 1, 1]
    ]

    expected_next_state = [
        [0, 0, 0],
        [1, 0, 0],
        [1, 1, 0]
    ]

    assert next_board_state(init_state) == expected_next_state

    init_state = [
        [1, 1, 1],
        [1, 0, 1],
        [1, 1, 1]
    ]

    expected_next_state = [
        [1, 0, 1],
        [0, 0, 0],
        [1, 0, 1]
    ]

    assert next_board_state(init_state) == expected_next_state

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    init_state = [
        [0, 1, 1],
        [0, 1, 1],
        [0, 1, 1]
    ]

    expected_next_state = [
        [0, 1, 1],
        [1, 0, 0],
        [0, 1, 1]
    ]

    assert next_board_state(init_state) == expected_next_state

    init_state = [
        [1, 1, 0],
        [1, 1, 0],
        [1, 1, 0]
    ]

    expected_next_state = [
        [1, 1, 0],
        [0, 0, 1],
        [1, 1, 0]
    ]

    assert next_board_state(init_state) == expected_next_state

    init_state = [
        [1, 0, 1],
        [1, 0, 1],
        [1, 0, 1]
    ]

    expected_next_state = [
        [0, 0, 0],
        [1, 0, 1],
        [0, 0, 0]
    ]

    assert next_board_state(init_state) == expected_next_state

    init_state = [
        [1, 1, 1],
        [1, 1, 1],
        [0, 0, 0]
    ]

    expected_next_state = [
        [1, 0, 1],
        [1, 0, 1],
        [0, 1, 0]
    ]

    assert next_board_state(init_state) == expected_next_state

    init_state = [
        [1, 1, 1],
        [0, 0, 0],
        [1, 1, 1]
    ]

    expected_next_state = [
        [0, 1, 0],
        [0, 0, 0],
        [0, 1, 0]
    ]

    assert next_board_state(init_state) == expected_next_state

    init_state = [
        [0, 0, 0],
        [1, 1, 1],
        [1, 1, 1]
    ]

    expected_next_state = [
        [0, 1, 0],
        [1, 0, 1],
        [1, 0, 1]
    ]

    assert next_board_state(init_state) == expected_next_state

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    init_state = [
        [1, 1, 1],
        [1, 1, 1],
        [1, 1, 1]
    ]

    expected_next_state = [
        [1, 0, 1],
        [0, 0, 0],
        [1, 0, 1]
    ]

    assert next_board_state(init_state) == expected_next_state


def test_toad() -> None:
    """Unit test for toad pattern."""
    init_state = [
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 1, 1, 1, 0],
        [0, 1, 1, 1, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0]
    ]

    expected_next_state = [
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 1, 0, 0],
        [0, 1, 0, 0, 1, 0],
        [0, 1, 0, 0, 1, 0],
        [0, 0, 1, 0, 0, 0],
        [0, 0, 0, 0, 0, 0]
    ]

    assert next_board_state(init_state) == expected_next_state


if __name__ == "__main__":

    test_state1()
    test_state2()
    test_state3()
    test_state4()
    test_state5()
    test_toad()
