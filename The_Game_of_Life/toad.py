"""Implementation of "Toad" pattern by initial state from text file."""

from game_of_life import *
from game_of_life_gui import *
from time import sleep


# Initialization the board
current_state = load_board_state("toad.txt")
rows = len(current_state)
cols = len(current_state[0])

master = Tk()  # parent widget
master.title("Toad")
distance = 15  # 15 pixels
canvas_width = rows * distance
canvas_height = cols * distance
canvas = Canvas(master, width=canvas_width, height=canvas_height)
canvas.pack()  # Organizes widgets in blocks before placing them in the parent w

cage(canvas, distance)

# Eternal life - infinite loop:
while True:
    render(current_state, canvas, distance)
    current_state = next_board_state(current_state)
    canvas.update()
    sleep(0.5)
    canvas.delete("alive")

mainloop()
